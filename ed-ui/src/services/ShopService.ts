import { http } from '@/utils';
import { DinozShop, Item } from '@/models';
export const ShopService = {
	getDinozFromDinozShop(): Promise<Array<DinozShop>> {
		return http()
			.get(`/shop/dinoz`)
			.then(res => {
				return Promise.resolve(res.data);
			})
			.catch(err => {
				return Promise.reject(err);
			});
	},
	getItemFromItemShop(): Promise<Array<Item>> {
		return http()
			.get(`/shop/item`)
			.then(res => {
				return Promise.resolve(res.data);
			})
			.catch(err => {
				return Promise.reject(err);
			});
	}
};
